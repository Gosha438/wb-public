!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! CROPPED !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


import 'dart:async';
import 'package:flutter/material.dart';
import 'package:whiterosebeauty/models/product.dart';
import 'package:whiterosebeauty/services/database.dart';
import 'package:whiterosebeauty/components/somefunctions.dart';
import 'package:whiterosebeauty/widgets/user_product_card.dart';

class Catalog extends StatefulWidget {
  const Catalog({Key key, this.categoryName}) : super(key: key);

  final String categoryName;

  @override
  _CatalogState createState() => _CatalogState();
}

class _CatalogState extends State<Catalog> {
  DataBaseService db = DataBaseService();
  // ignore: cancel_subscriptions
  StreamSubscription<List<Product>> productsStreamSubscription;
  List<Product> products = List<Product>.empty(growable: true);

  double filterHeight = 0.0;
  bool showOveredProducts = false;

  String filterText = '';
  String filterTitle = '';
  String mainCategory = 'Все категории';

  @override
  void initState() {
    filter(clear: true);
    super.initState();
  }

  @override
  void dispose() {
    checkStreamSubscription(streamName: productsStreamSubscription);
    super.dispose();
  }

  void filter({bool clear = false}) {
    if (clear) {
      showOveredProducts = false;
      mainCategory = 'Все категории';
    }
    loadProducts();
  }

  Future<void> loadProducts() async {
    final stream = db.getProducts(category: widget.categoryName);

    productsStreamSubscription = stream.listen((List<Product> data) {
      setState(() {
        products = data;
        products.sort((a, b) => b.quantity.compareTo(a.quantity));
        filterText = showOveredProducts
            ? 'Закончившиеся (${products.length})'
            : 'В наличии (${products.length})';
        filterHeight = 0;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    
    final productsList = Expanded(
        child: products.isNotEmpty
            ? GridView.count(
                childAspectRatio: 0.65,
                crossAxisCount: 2,
                crossAxisSpacing: 6,
                mainAxisSpacing: 6,
                children: List.generate(products.length, (i) {
                  return UserProductCard(
                    product: products[i],
                    onTap: () {},
                  );
                }),
              )
            : const Center(
                child: Text('Нет таких товаров'),
              ));
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.categoryName),
      ),
      body: Column(
        children: <Widget>[
          // filterInfo,
          // filterForm,
          productsList
        ],
      ),
    );
  }
}
