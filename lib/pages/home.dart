import 'dart:async';

import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
import 'package:fluttericon/linearicons_free_icons.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:provider/provider.dart';
import 'package:whiterosebeauty/components/somefunctions.dart';
import 'package:whiterosebeauty/models/product.dart';
import 'package:whiterosebeauty/models/user.dart';
import 'package:whiterosebeauty/pages/auth.dart';
import 'package:whiterosebeauty/pages/cart.dart';
import 'package:whiterosebeauty/pages/categories.dart';
import 'package:whiterosebeauty/pages/favorites.dart';
import 'package:whiterosebeauty/pages/news.dart';
import 'package:whiterosebeauty/pages/user_info.dart';
import 'package:whiterosebeauty/services/database.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  DataBaseService db = DataBaseService();
  // ignore: cancel_subscriptions
  StreamSubscription<List<Product>> cartProductsStreamSubscription;

  @override
  void dispose() {
    checkStreamSubscription(streamName: cartProductsStreamSubscription);
    super.dispose();
  }


  PersistentTabController tabController;

  int sectionIndex = 0;

  int numCartProducts = 0;

  @override
  Widget build(BuildContext context) {
    final UserFB userFB = Provider.of<UserFB>(context);
    final bool isLoggedIn = userFB != null;

    Stream<List<dynamic>> streamCart;
    isLoggedIn == true
        ? streamCart = db.getUserCart(userFB)
        : streamCart = null;
    var cart = List<Product>.empty(growable: true);

    List<Widget> buildScreens() {
      return [
        NewsPage(),
        Favorites(),
        Categories(),
        Cart(),
        UserInfo(),
      ];
    }

    List<PersistentBottomNavBarItem> navBarsItems() {
      return [
        PersistentBottomNavBarItem(
          routeAndNavigatorSettings: RouteAndNavigatorSettings(),
          icon: Icon(LineariconsFree.gift),
          title: 'Новости',
          activeColorPrimary: Colors.black,
          inactiveColorPrimary: Colors.grey,
        ),
        PersistentBottomNavBarItem(
          icon: Icon(LineariconsFree.heart_1),
          title: 'Избранное',
          activeColorPrimary: Colors.black,
          inactiveColorPrimary: Colors.grey,
        ),
        PersistentBottomNavBarItem(
          icon: Icon(LineariconsFree.store),
          title: 'Каталог',
          activeColorPrimary: Colors.black,
          inactiveColorPrimary: Colors.grey,
        ),
        PersistentBottomNavBarItem(
          icon: StreamBuilder<List<dynamic>>(
              stream: streamCart,
              builder: (BuildContext context,
                  AsyncSnapshot<List<dynamic>> snapshotCart) {
                if (snapshotCart.data != null) {
                  cart =
                      snapshotCart.data.map((w) => Product.fromMap(w)).toList();
                }
                numCartProducts = getProductsCount(cart);
                return Badge(
                  animationType: BadgeAnimationType.scale,
                  animationDuration: Duration(milliseconds: 100),
                  badgeColor: Colors.black,
                  showBadge: numCartProducts > 0,
                  badgeContent: Text(
                    '$numCartProducts',
                    textScaleFactor: 0.85,
                    style: TextStyle(color: Colors.white),
                  ),
                  child: Icon(LineariconsFree.cart),
                );
              }),
          title: 'Корзина',
          activeColorPrimary: Colors.black,
          inactiveColorPrimary: Colors.grey,
        ),
        PersistentBottomNavBarItem(
          icon: Icon(LineariconsFree.user_1),
          title: 'Профиль',
          activeColorPrimary: Colors.black,
          inactiveColorPrimary: Colors.grey,
        ),
      ];
    }

    return !isLoggedIn
        ? AuthPage()
        : PersistentTabView(context,
            controller: tabController,
            screens: buildScreens(),
            items: navBarsItems(),
            resizeToAvoidBottomInset: true,
            popAllScreensOnTapOfSelectedTab: false,
            navBarStyle: NavBarStyle.style13,
            navBarHeight: 50,
            decoration: NavBarDecoration(
                border: Border(top: BorderSide(color: Colors.black12))));
  }
}
