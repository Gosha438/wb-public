import 'package:flutter/material.dart';
import 'package:flutter_multi_formatter/flutter_multi_formatter.dart';
import 'package:whiterosebeauty/services/auth.dart';
import 'package:whiterosebeauty/widgets/wide_button.dart';

class AuthPage extends StatefulWidget {
  @override
  _AuthPageState createState() => _AuthPageState();
}

class _AuthPageState extends State<AuthPage> {
  final TextEditingController phoneNumberController = TextEditingController();
  AuthService authService = AuthService();

  bool isValid = false;
  bool showError = false;
  String phoneNumber = '';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Center(
          child: Stack(
            alignment: AlignmentDirectional.bottomCenter,
            children: [
              SizedBox(
                height: 25,
                child: Text('При входе вы соглашаетесь с условиями'),
              ),
              SizedBox(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      padding: EdgeInsets.symmetric(horizontal: 15),
                      child: Column(
                        children: [
                          Text(
                            'войти или зарегистрироваться',
                            style: TextStyle(
                                fontSize: 30, fontWeight: FontWeight.bold),
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Text(
                              'мы отправим на номер SMS-сообщение с кодом подтверждения'),
                          SizedBox(
                            height: 75,
                            child: TextFormField(
                              inputFormatters: [
                                MaskedInputFormatter('(###)###-##-##'),
                              ],
                              keyboardType: TextInputType.number,
                              controller: phoneNumberController,
                              decoration: InputDecoration(
                                hintText: '( . . . ) . . . - . . - . .',
                                prefixStyle: TextStyle(color: Colors.black),
                                prefix: SizedBox(
                                  child: Text(
                                    '+7',
                                    style: TextStyle(fontSize: 16),
                                  ),
                                ),
                                errorText: showError
                                    ? 'Введите действительный номер'
                                    : null,
                              ),
                              onChanged: (text) {
                                phoneNumber = text
                                    .replaceAll(' ', '')
                                    .replaceAll('-', '')
                                    .replaceAll('(', '')
                                    .replaceAll(')', '');

                                if (phoneNumber.length == 10 ||
                                    phoneNumber.isEmpty) {
                                  if (phoneNumber.length == 10) isValid = true;
                                  showError = false;
                                } else {
                                  showError = true;
                                  isValid = false;
                                }
                                setState(() {});
                              },
                            ),
                          ),
                        ],
                      ),
                    ),
                    WideButton(
                      text: 'получить код',
                      isActive: isValid,
                      onTap: () {
                        if (isValid) {
                          authService.verifyPhone(
                              phoneNumber: '+7${phoneNumberController.text}',
                              context: context);
                        }
                      },
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
