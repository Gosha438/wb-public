import 'dart:async';

import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:provider/provider.dart';
import 'package:whiterosebeauty/components/constants.dart';
import 'package:whiterosebeauty/components/somefunctions.dart';
import 'package:whiterosebeauty/models/orderproducts.dart';
import 'package:whiterosebeauty/models/user.dart';
import 'package:whiterosebeauty/pages/home.dart';
import 'package:whiterosebeauty/services/auth.dart';

Future main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(WhiteRoseBeauty());
}

class WhiteRoseBeauty extends StatefulWidget {
  @override
  _WhiteRoseBeautyState createState() => _WhiteRoseBeautyState();
}

class _WhiteRoseBeautyState extends State<WhiteRoseBeauty> {
  // ignore: cancel_subscriptions
  StreamSubscription<UserFB> userStream;
  Stream<UserFB> userDataStream;

  StreamSubscription<UserFB> setUserData() {
    final auth = AuthService();
    return auth.currentUser.listen((user) {
      userDataStream = auth.getCurrentUserWithData(user);
      setState(() {});
    });
  }

  @override
  void initState() {
    super.initState();
    userStream = setUserData();
  }

  @override
  void dispose() {
    super.dispose();
    checkStreamSubscription(streamName: userStream);
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (context) => OrderProductsProvider(),
        ),
        StreamProvider<UserFB>.value(
          initialData: null,
          value: userDataStream,
        ),
      ],
      child: MaterialApp(
        localizationsDelegates: [GlobalMaterialLocalizations.delegate],
        supportedLocales: [const Locale('ru')],
        theme: ThemeData(
          primarySwatch: mColor,
          primaryColor: Colors.white,
          // textTheme: TextTheme(),
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        home: HomePage(),
      ),
    );
  }
}
