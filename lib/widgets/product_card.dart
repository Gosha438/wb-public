import 'package:extended_image/extended_image.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:whiterosebeauty/components/constants.dart';
import 'package:whiterosebeauty/components/somefunctions.dart';
import 'package:whiterosebeauty/models/product.dart';
import 'package:whiterosebeauty/models/user.dart';
import 'package:whiterosebeauty/services/database.dart';
import 'package:whiterosebeauty/widgets/favorite_icon.dart';

class ProductCard extends StatefulWidget {
  const ProductCard({Key key, this.onTap, this.product}) : super(key: key);

  final Function onTap;
  final Product product;

  @override
  _ProductCardState createState() => _ProductCardState();
}

class _ProductCardState extends State<ProductCard> {
  @override
  Widget build(BuildContext context) {
    UserFB userFB = Provider.of<UserFB>(context);

    bool isFavorite =
        isFavoriteProduct(user: userFB, article: widget.product.article);

    return GridTile(
      header: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          SizedBox(
            width: 40,
            height: 40,
            child: FavoriteIcon(
              isFavorite: isFavorite,
              isNotFavoriteAction: () async {
                await DataBaseService()
                    .addProductToFavorites(
                        user: userFB, article: widget.product.article)
                    .then((v) {
                  if (mounted) {
                    setState(() {});
                  }
                });
              },
              isFavoriteAction: () async {
                await DataBaseService()
                    .removeProductFromFavorites(
                        user: userFB, article: widget.product.article)
                    .then((v) {
                  if (mounted) {
                    setState(() {});
                  }
                });
              },
            ),
          ),
        ],
      ),
      child: InkWell(
        onTap: () {
          widget.onTap();
        },
        child: Column(
          children: <Widget>[
            ExtendedImage.network(
              widget.product.img,
              shape: BoxShape.rectangle,
              borderRadius: BorderRadius.all(Radius.circular(10)),
              width: MediaQuery.of(context).size.width / 2 - 3,
              height: MediaQuery.of(context).size.width / 2 - 3,
              fit: BoxFit.cover,
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    height: 6,
                  ),
                  Text(
                    widget.product.title,
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  Text(widget.product.manufacturer),
                  SizedBox(
                    height: 5,
                  ),
                  SizedBox(
                    height: 25,
                    child: Row(
                      children: [
                        Text(
                          '${widget.product.cost} $rubleSymbol',
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                        // Text(
                        //   '    ${widget.product.article}',
                        //   style: TextStyle(fontWeight: FontWeight.bold),
                        // ),
                        Flexible(
                          child: FractionallySizedBox(
                            widthFactor: 1,
                          ),
                        ),
                        IconButton(
                          padding: EdgeInsets.zero,
                          icon: Icon(Icons.add),
                          onPressed: () async {
                            await DataBaseService().addProductToCart(
                                user: userFB, product: widget.product);
                          },
                        )
                      ],
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
