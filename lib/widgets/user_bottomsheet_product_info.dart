import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:whiterosebeauty/components/constants.dart';
import 'package:extended_image/extended_image.dart';
import 'package:whiterosebeauty/components/somefunctions.dart';
import 'package:whiterosebeauty/models/product.dart';
import 'package:whiterosebeauty/models/user.dart';
import 'package:whiterosebeauty/services/database.dart';
import 'package:whiterosebeauty/widgets/favorite_icon.dart';
import 'package:whiterosebeauty/widgets/wide_button.dart';

class UserProductInfo extends StatefulWidget {
  const UserProductInfo({Key key, this.product, this.isBuyable})
      : super(key: key);

  final Product product;
  final bool isBuyable;

  @override
  _UserProductInfoState createState() => _UserProductInfoState();
}

class _UserProductInfoState extends State<UserProductInfo> {
  @override
  Widget build(BuildContext context) {
    UserFB userFB = Provider.of<UserFB>(context);
    var db = DataBaseService();
    bool isFavorite =
        isFavoriteProduct(user: userFB, article: widget.product.article);

    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: () => Navigator.of(context).pop(),
      child: DraggableScrollableSheet(
        initialChildSize: 0.9,
        maxChildSize: 0.9,
        minChildSize: 0.6,
        builder: (context, scrollController) {
          return Container(
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.vertical(top: Radius.circular(20)),
            ),
            child: Stack(
              alignment: AlignmentDirectional.bottomCenter,
              children: [
                ListView(
                  controller: scrollController,
                  children: [
                    Stack(
                      alignment: Alignment.topRight,
                      children: [
                        ExtendedImage.network(
                          widget.product.img,
                          shape: BoxShape.rectangle,
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(20),
                              topRight: Radius.circular(20)),
                          width: MediaQuery.of(context).size.width,
                          height: MediaQuery.of(context).size.width,
                          fit: BoxFit.cover,
                        ),
                        FavoriteIcon(
                          isBig: true,
                          isFavorite: isFavorite,
                          isNotFavoriteAction: () async {
                            await db
                                .addProductToFavorites(
                                    user: userFB,
                                    article: widget.product.article)
                                .then((v) {
                              setState(() {});
                            });
                          },
                          isFavoriteAction: () async {
                            await db
                                .removeProductFromFavorites(
                                    user: userFB,
                                    article: widget.product.article)
                                .then((v) {
                              setState(() {});
                            });
                          },
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Column(
                      children: [
                        Text('Производитель: ${widget.product.manufacturer}'),
                        Container(
                          margin: EdgeInsets.all(20),
                          child: Text(widget.product.subtitle),
                        ),
                        SizedBox(
                          height: 75,
                        ),
                      ],
                    ),
                  ],
                ),
                Container(
                  height: 95,
                  width: MediaQuery.of(context).size.width,
                  color: Colors.white,
                  child: Column(
                    children: [
                      Divider(
                        height: 1,
                      ),
                      Container(
                        height: 34,
                        padding: EdgeInsets.symmetric(horizontal: 15),
                        child: Row(
                          children: [
                            Text(
                              widget.product.title,
                              style: TextStyle(fontWeight: FontWeight.bold),
                            ),
                            Flexible(
                              child: FractionallySizedBox(
                                widthFactor: 1,
                              ),
                            ),
                            Text(
                              '${widget.product.cost} $rubleSymbol',
                              style: TextStyle(fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                      ),
                      if (widget.isBuyable)
                        WideButton(
                          isActive: widget.product.quantity > 0,
                          text: widget.product.quantity > 0
                              ? 'добавить в корзину'
                              : 'товар закончился',
                          onTap: () async {
                            await db
                                .addProductToCart(
                                    user: userFB, product: widget.product)
                                .whenComplete(() {
                              buildToast(
                                  '${widget.product.title} добавлен в корзину');
                            });
                          },
                        ),
                    ],
                  ),
                ),
              ],
            ),
          );
        },
      ),
    );
  }
}
