import 'package:flutter/material.dart';
import 'package:whiterosebeauty/components/constants.dart';

class FavoriteIcon extends StatelessWidget {
  const FavoriteIcon({
    Key key,
    this.isFavorite,
    this.isFavoriteAction,
    this.isNotFavoriteAction,
    this.isBig,
  }) : super(key: key);

  final bool isFavorite;
  final Function isFavoriteAction;
  final Function isNotFavoriteAction;
  final bool isBig;

  @override
  Widget build(BuildContext context) {
    // return Container(
    //   width: isBig == true ? 40 : 25,
    //   height: isBig == true ? 40 : 25,
    //   child: CircleAvatar(
    //     backgroundColor: Colors.white,
    //     child: Center(
    //       child: IconButton(
    //         padding: EdgeInsets.all(0),
    //         color: isFavorite == false ? Colors.black : Colors.red,
    //         icon: isFavorite == false ? iconNotFavorite : iconFavorite,
    //         onPressed: () async {
    //           if (isFavorite == false) {
    //             isNotFavoriteAction();
    //           } else {
    //             isFavoriteAction();
    //           }
    //         },
    //       ),
    //     ),
    //   ),
    // );
    return SizedBox(
      width: isBig == true ? 40 : 30,
      height: isBig == true ? 40 : 30,
      child: Stack(
        alignment: AlignmentDirectional.center,
        children: [
          if (isBig == true)
            CircleAvatar(
              backgroundColor: Colors.white,
            )
          else
            Icon(
              Icons.favorite,
              size: 26,
              color: Colors.white,
            ),
          IconButton(
            padding: EdgeInsets.all(0),
            color: isFavorite == false ? Colors.black : Colors.red,
            icon: isFavorite == false ? iconNotFavorite : iconFavorite,
            onPressed: () async {
              if (isFavorite == false) {
                isNotFavoriteAction();
              } else {
                isFavoriteAction();
              }
            },
          ),
        ],
      ),
    );
  }
}
