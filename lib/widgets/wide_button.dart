import 'package:flutter/material.dart';

class WideButton extends StatelessWidget {
  const WideButton({Key key, this.onTap, this.text, this.isActive})
      : super(key: key);

  final Function onTap;
  final String text;
  final bool isActive;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
      height: 50,
      color: Colors.white,
      child: InkWell(
        onTap: () {
          if (isActive) onTap();
        },
        child: Container(
          // margin: EdgeInsets.all(5),
          color: isActive ? Colors.black : Colors.grey,
          // padding: EdgeInsets.symmetric(horizontal: 20),

          child: Center(
            child: Text(
              text,
              style: TextStyle(
                fontWeight: FontWeight.bold,
                color: Colors.white,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
