

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! CROPPED !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:path_provider/path_provider.dart';
import 'package:whiterosebeauty/models/order.dart';
import 'package:whiterosebeauty/models/product.dart';
import 'package:whiterosebeauty/models/user.dart';

dynamic getListProducts(Order order) {
  if (order.products.isNotEmpty) {
    return order.products.map((e) => e.article).toList();
  } else {
    return List<dynamic>.empty(growable: true);
  }
}

dynamic getListProductsCost(List<Product> products) {
  if (products.isNotEmpty) {
    return products.map((e) => e.cost).toList();
  } else {
    return List<dynamic>.empty(growable: true);
  }
}

dynamic getProductsCount(List<Product> products) {
  var count = 0;
  for (var p in products) {
    count += p.quantity;
  }
  return count;
}

dynamic getProductsSum(List<Product> products) {
  var sum = 0;
  for (var p in products) {
    sum += p.quantity * p.cost;
  }
  return sum;
}

String getShortDate(Order order) {
  return DateFormat('dd.MM.yyyy | kk:mm').format(order.orderDate);
}

String getUserShortDate(Order order) {
  return DateFormat('dd.MM.yyyy').format(order.orderDate);
}

void checkStreamSubscription({StreamSubscription streamName}) {
  if (streamName != null) {
    streamName.cancel();
  }
}

num max(num val1, num val2) {
  return val1 >= val2 ? val1 : val2;
}

Future<File> imageToFile({String imageName, String ext}) async {
  final bytes = await rootBundle.load('assets/$imageName.$ext');
  final String tempPath = (await getTemporaryDirectory()).path;
  final File file = File('$tempPath/image.png');
  await file.writeAsBytes(
      bytes.buffer.asUint8List(bytes.offsetInBytes, bytes.lengthInBytes));
  return file;
}

Future<bool> buildToast(String message,
    {Color backgroundColor = Colors.red, Color textColor = Colors.white}) {
  return Fluttertoast.showToast(
      msg: message,
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.BOTTOM,
      // timeInSecForIos: 2,
      backgroundColor: backgroundColor,
      textColor: textColor,
      fontSize: 16.0);
}

Future<String> scanBar() async {
  return FlutterBarcodeScanner.scanBarcode(
    '#00000000',
    'Отмена',
    true,
    ScanMode.BARCODE,
  );
}

bool isFavoriteProduct({UserFB user, String article}) {
  return user.userData.favorites.contains(article);
}

int productIdinCart({UserFB user, String article}) {
  return user.userData.cartProducts
      .indexWhere((product) => product.article == article);
}
