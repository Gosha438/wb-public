class Adress {
  String region;
  String city;
  String street;
  String house;
  String flat;
  String index;

  // ignore: sort_constructors_first
  Adress({
    this.region,
    this.city,
    this.street,
    this.house,
    this.flat,
    this.index,
  });

  Map<String, dynamic> toMap() {
    return {
      'region': region,
      'city': city,
      'street': street,
      'house': house,
      'flat': flat,
      'index': index,
    };
  }

  // ignore: sort_constructors_first
  Adress.fromMap(Map<String, dynamic> data) {
    region = data['region'];
    city = data['city'];
    street = data['street'];
    house = data['house'];
    flat = data['flat'];
    index = data['index'];
  }
}
