import 'package:firebase_auth/firebase_auth.dart';
import 'package:whiterosebeauty/models/contacts.dart';
import 'package:whiterosebeauty/models/notifications.dart';
import 'package:whiterosebeauty/models/product.dart';

import 'adress.dart';

class UserFB {
  UserFB({
    this.id,
    this.userData,
  });

  UserFB.fromFirebase(User user) {
    id = user.uid;
  }

  String id;
  UserData userData;
}

class UserData {
  UserData({
    this.favorites,
    this.cartProducts,
    this.orders,
    this.contacts,
    this.adress,
    this.notifications,
  });

  UserData.fromMap(Map<String, dynamic> data) {
    if (data['favorites'] == null) {
      favorites = List<dynamic>.empty(growable: true);
    } else {
      favorites = data['favorites'] as List;
    }

    if (data['cartProducts'] == null) {
      cartProducts = List<dynamic>.empty(growable: true);
    } else {
      cartProducts = (data['cartProducts'] as List)
          .map((w) => Product.fromMap(w))
          .toList();
    }

    if (data['orders'] == null) {
      orders = List<dynamic>.empty(growable: true);
    } else {
      orders = data['orders'] as List;
    }

    if (data['contacts'] == null) {
      contacts = Contacts();
    } else {
      contacts = Contacts.fromMap(data['contacts']);
    }

    if (data['adress'] == null) {
      adress = Adress();
    } else {
      adress = Adress.fromMap(data['adress']);
    }

    if (data['notifications'] == null) {
      notifications = Notifications();
    } else {
      notifications = Notifications.fromMap(data['notifications']);
    }
  }

  List<dynamic> favorites;
  List<dynamic> cartProducts;
  List<dynamic> orders;
  Contacts contacts;
  Adress adress;
  Notifications notifications;

  Map<String, dynamic> toMap() {
    return {
      'favorites': favorites,
      'cartProducts': cartProducts,
      'orders': orders,
      'contacts': contacts,
      'adress': adress,
      'notifications': notifications,
    };
  }
}
